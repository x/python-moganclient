
=======================
Baremetalcompute server
=======================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server create

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server show

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server list

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server delete

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server add floating ip

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server add port

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server lock

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server netinfo

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server reboot

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server remove floating ip

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server remove port

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server set

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server start

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server stop

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server unlock

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server unset

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server rebuild

=============================
Baremetalcompute server group
=============================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server group create

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server group show

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server group list

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server group delete

=======================
BaremetalCompute flavor
=======================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute flavor *

==========================
BaremetalCompute aggregate
==========================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute aggregate *

========================
BaremetalCompute keypair
========================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute keypair *

=====================
BaremetalCompute node
=====================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute node *

==================================
BaremetalCompute availability zone
==================================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute availability zone *

==================================
BaremetalCompute manageable server
==================================

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute manageable server list

.. autoprogram-cliff:: openstack.baremetal_compute.v1
    :command: baremetalcompute server manage
